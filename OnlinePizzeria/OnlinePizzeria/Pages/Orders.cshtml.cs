using Microsoft.AspNetCore.Mvc.RazorPages;

using OnlinePizzeria.Data;
using OnlinePizzeria.Models;

namespace OnlinePizzeria.Pages
{
    public class OrdersModel : PageModel
    {
        private readonly ApplicationDbContext context;
        public List<PizzaOrder> PizzaOrders = new List<PizzaOrder>();

        public OrdersModel(ApplicationDbContext context)
        {
            this.context = context;
        }

        public void OnGet()
        {
            PizzaOrders = context.PizzaOrders.ToList();
        }
    }
}
