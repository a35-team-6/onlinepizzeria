using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using OnlinePizzeria.Models;

namespace OnlinePizzeria.Pages.Forms
{
    public class CustomPizzaModel : PageModel
    {
        [BindProperty]
        public PizzasModel Pizza { get; set; }

        public float PizzaPrice { get; set; }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            this.PizzaPrice = this.Pizza.BasePrise;

            if (this.Pizza.TomatoSauce) this.PizzaPrice++;
            if (this.Pizza.Cheese) this.PizzaPrice++;
            if (this.Pizza.Peperoni) this.PizzaPrice++;
            if (this.Pizza.Mushrooms) this.PizzaPrice++;
            if (this.Pizza.Tuna) this.PizzaPrice++;
            if (this.Pizza.Pineapple) this.PizzaPrice++;
            if (this.Pizza.Ham) this.PizzaPrice++;
            if (this.Pizza.Beef) this.PizzaPrice++;

            return RedirectToPage("/Checkout/Checkout", new { this.Pizza.PizzaName, this.PizzaPrice });
        }
    }
}
