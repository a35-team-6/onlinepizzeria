using Microsoft.AspNetCore.Mvc.RazorPages;

using OnlinePizzeria.Models;

namespace OnlinePizzeria.Pages
{
    public class PizzaModel : PageModel
    {
        public List<PizzasModel> fakePizzaDB = new List<PizzasModel>()
        {
            new PizzasModel(){
                ImageTitle="Margerita",
                PizzaName="Margerita",
                BasePrise=2,
                TomatoSauce=true,
                Cheese=true,
                FinalPrice=4},
            new PizzasModel(){
                ImageTitle="Carbonara",
                PizzaName="Carbonara",
                BasePrise=3,
                TomatoSauce=true,
                Cheese=true,
                Peperoni=true,
                Mushrooms=true,
                FinalPrice=7}
        };

        public void OnGet()
        {
        }
    }
}
