using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using OnlinePizzeria.Data;
using OnlinePizzeria.Models;

namespace OnlinePizzeria.Pages.Checkout
{
    [BindProperties(SupportsGet = true)]
    public class CheckoutModel : PageModel
    {
        private readonly ApplicationDbContext context;

        public CheckoutModel(ApplicationDbContext context)
        {
            this.context = context;
        }

        public string PizzaName { get; set; }

        public float PizzaPrice { get; set; }

        public string ImageTitle { get; set; }


        public void OnGet()
        {
            if (string.IsNullOrWhiteSpace(this.PizzaName))
            {
                this.PizzaName = "Custom";
            }
            if (string.IsNullOrWhiteSpace(this.ImageTitle))
            {
                this.ImageTitle = "Create";
            }

            PizzaOrder pizzaOrder = new PizzaOrder();
            pizzaOrder.PizzaName = this.PizzaName;
            pizzaOrder.BazePrice = this.PizzaPrice;

            context.PizzaOrders.Add(pizzaOrder);
            context.SaveChanges();
        }
    }
}
